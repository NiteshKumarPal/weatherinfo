//
//  WeatherInfo.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 20/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import Foundation
import ObjectMapper

class WeatherInfo: Mappable {
    var weatherInfoId = ""
    var weatherInfoCityName = ""
    var weatherInfoVisiblity: Double = 0
    
    var coordinate = Coordinate()
    
    var weatherCondition: [WeatherCondition] = [WeatherCondition]()
    
    //Weather Main
    var weatherInfoTemprature = 0
    var weatherInfoMinTemprature = 0
    var weatherInfoMaxTemprature = 0
    var weatherInfoHumidity = 0
    var weatherInfoPressure = 0
    
    //Weather System
    var weatherInfoSystemCountry = ""
    var weatherInfoSystemSunset: Double = 0
    var weatherInfoSystemSunrise: Double = 0

    //Weather Wind
    var weatherInfoWindSpeed: Double = 0
    var weatherInfoWindDegree: Double = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        weatherInfoId  <- map[WeatherApiKeys.ID]
        weatherInfoCityName <- map[WeatherApiKeys.NAME]
        weatherInfoVisiblity <- map[WeatherApiKeys.VISIBLITY]
        coordinate <- map[WeatherApiKeys.COORDINATE]
        
        //Weather Main
        weatherInfoTemprature <- map[WeatherApiKeys.MAIN_TEMP]
        weatherInfoMinTemprature <- map[WeatherApiKeys.MAIN_TEMP_MIN]
        weatherInfoMaxTemprature <- map[WeatherApiKeys.MAIN_TEMP_MAX]
        weatherInfoHumidity <- map[WeatherApiKeys.MAIN_HUMIDITY]
        weatherInfoPressure <- map[WeatherApiKeys.MAIN_PRESSURE]
        
        //Weather System
        weatherInfoSystemCountry <- map[WeatherApiKeys.SYS_COUNTRY]
        weatherInfoSystemSunset <- map[WeatherApiKeys.SYS_SUNSET]
        weatherInfoSystemSunrise <- map[WeatherApiKeys.SYS_SUNRISE]

        weatherCondition <- map[WeatherApiKeys.WEATHER]
        
        //Weather Wind
        weatherInfoWindSpeed <- map[WeatherApiKeys.WIND_SPEED]
        weatherInfoWindDegree <- map[WeatherApiKeys.WIND_DEG]
    }
}

class WeatherCondition: Mappable {
    var weatherConditionMain = ""
    var weatherConditionDescription = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        weatherConditionMain <- map[WeatherApiKeys.WEATHER_MAIN]
        weatherConditionDescription <- map[WeatherApiKeys.WEATHER_DESCRIPTION]
    }
}

class Coordinate: Mappable {
    var lattitude: Double = 0
    var longitude: Double = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        lattitude <- map[WeatherApiKeys.LAT]
        longitude <- map[WeatherApiKeys.LONG]
    }

}


