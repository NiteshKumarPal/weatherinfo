//
//  Constants.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 21/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import Foundation
import UIKit

typealias StoryBorads = Constants.StoryBorads
typealias ViewControllerStoryboardIds = Constants.ViewControllerStoryboardIds
typealias WeatherApiKeys = Constants.WeatherApiKeys
typealias URLs = Constants.URLs
typealias ToastConstant = Constants.ToastConstant
typealias CommonAlertTexts = Constants.CommonAlertTexts
typealias DateFormatters = Constants.DateFormatters
typealias SpecialCharacteres = Constants.SpecialCharacteres

struct Constants {
    
    static let BASE_URL = "http://api.openweathermap.org"
    static let API_KEY = "7f3d9ae5f8a73a219dd2b69a73868ecd"
    
    struct URLs {
        //This is function which is treating like dynamic constant as per varied city ids
        //city id can be one or more than one comma separated
        static func CITY_WEATHER_API(cityId: String) -> String {
            return "\(BASE_URL)/data/2.5/group?id=\(cityId)&units=metric&appid=\(API_KEY)"
        }
    }
    
    struct SpecialCharacteres {
        static let DEGREE = "°"
        static let SPACES = " "
        static let SLASH = "/"
    }
    
    struct ToastConstant {
        static let TOAST_DURATION: Double = 3.0
        static let CHECK_INTERNET = "Please connect Internet"
    }
    
    struct CommonAlertTexts {
        static let OK = "Ok"
        static let Cancel = "Cancel"
    }
    
    struct StoryBorads {
        static let MAIN = UIStoryboard(name: "Main", bundle: nil)
    }
    
    struct ViewControllerStoryboardIds {
        static let WEATHER_INFO_CONTROLLER = "WeatherInfoController"
        static let WEATHER_DETAIL_VIEW_CONTROLLER = "WeatherDetailViewController"
    }
    
    struct WeatherApiKeys {
        static let SYS_COUNTRY = "sys.country"
        static let SYS_SUNRISE = "sys.sunrise"
        static let SYS_SUNSET = "sys.sunset"
        static let WEATHER = "weather"
        static let WEATHER_MAIN = "main"
        static let WEATHER_DESCRIPTION = "description"
        static let MAIN_TEMP = "main.temp"
        static let MAIN_PRESSURE = "main.pressure"
        static let MAIN_HUMIDITY = "main.humidity"
        static let MAIN_TEMP_MIN = "main.temp_min"
        static let MAIN_TEMP_MAX = "main.temp_max"
        static let VISIBLITY = "visibility"
        static let WIND = "wind"
        static let WIND_SPEED = "wind.speed"
        static let WIND_DEG = "wind.deg"
        static let ID = "id"
        static let NAME = "name"
        static let LIST = "list"
        static let LAT = "lat"
        static let LONG = "lon"
        static let COORDINATE = "coord"
    }
    
    struct DateFormatters {
        static let DATE_FORMAT_DD_MM_YYYY = "dd-MM-yyyy"
        static let DATE_FORMAT_DD_MM_YY_HH_MM_AM_PM = "dd-MM-yyyy hh:mm a"
    }
}
