//
//  ViewController.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 20/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import UIKit

class WeatherInfoController: UIViewController, WeatherInfoViewModelDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var weatherInfoViewModel: WeatherInfoViewModel! = WeatherInfoViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set delegate for reloading table after new data recieved
        weatherInfoViewModel.weatherInfoViewModelDelegate = self
        
        //Initial UI Setup
        uiSetup()
        
        //Get weather info from api and update ui
        weatherInfoViewModel.getWeatherInfoList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func uiSetup() {
        tableView.separatorColor = UIColor.clear
        addRefreshController()
    }

    //MARK: WeatherInfoViewModelDelegate
    func relaodWeatherInfo() {
        tableView.reloadData()
    }
}

//MARK: Tableview datasource and delegates
extension WeatherInfoController: UITableViewDataSource, UITableViewDelegate {
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherInfoViewModel.weatherInfoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let weatherInfoCell = tableView.dequeueReusableCell(withIdentifier: WeatherInfoTableViewCell.IDENTIIER, for: indexPath) as! WeatherInfoTableViewCell
        
        weatherInfoCell.weatherInfo = weatherInfoViewModel.weatherInfoList[indexPath.row]
        
        return weatherInfoCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let weatherDetailViewController = StoryBorads.MAIN.instantiateViewController(withIdentifier: ViewControllerStoryboardIds.WEATHER_DETAIL_VIEW_CONTROLLER) as? WeatherDetailViewController
        
        weatherDetailViewController?.weatherDetailViewModel.weatherInfo = weatherInfoViewModel.weatherInfoList[indexPath.row]
        
        navigationController?.pushViewController(weatherDetailViewController!, animated: true)
    }
}

//MARK: Refresh controller methods
extension WeatherInfoController {
    
    /// By calling this method you can initialise refreshControl and setup its configuration
    func addRefreshController() {
        if refreshControl == nil {
            refreshControl = UIRefreshControl()
            addRefreshControllerForScrollView(scrollView: tableView,
                                              refreshControl: refreshControl,
                                              massage: "",
                                              selector: #selector(refresh(_:)))
        }
    }
    
    /// This method will be invoked when pull to refresh will be triggered
    ///
    /// - Parameter sender: sender is UIRefreshControl
    func refresh(_ sender: UIRefreshControl) {
        //Remove pull to refresh loader
        refreshControl?.endRefreshing()
        
        //API call for fetching notification for todays date
        weatherInfoViewModel.getWeatherInfoList()
    }
}

