//
//  WeatherInfoTableViewCell.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 20/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import UIKit

class WeatherInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var labelCityName: UILabel!
    @IBOutlet weak var labelCityTempratuew: UILabel!
    
    static let IDENTIIER = "WeatherInfoTableViewCell"
    
    //weatherInfo model object is responsible for ui of present cell
    var weatherInfo: WeatherInfo! {
        didSet {
            labelCityName.text = weatherInfo.weatherInfoCityName
            labelCityTempratuew.text = String(weatherInfo.weatherInfoTemprature) + SpecialCharacteres.DEGREE
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
