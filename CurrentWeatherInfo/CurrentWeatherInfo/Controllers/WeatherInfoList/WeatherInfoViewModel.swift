//
//  WeatherInfoViewModel.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 23/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import Foundation

@objc protocol WeatherInfoViewModelDelegate {
   @objc optional func relaodWeatherInfo()
}

class WeatherInfoViewModel {
    
    var weatherInfoList: [WeatherInfo]! = [WeatherInfo]()
    weak var weatherInfoViewModelDelegate: WeatherInfoViewModelDelegate?
    
    /// Get weather Info from server and update UI
    func getWeatherInfoList() {
        ModelApiUtilityInstance.getWeatherInfoListForCities { (weatherInfoList) in
            printForDebugMode(weatherInfoList)
            self.weatherInfoList = weatherInfoList
            self.weatherInfoViewModelDelegate?.relaodWeatherInfo?()
        }
    }
}
