//
//  WeatherDetailViewModel.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 21/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import Foundation
import UIKit

class WeatherDetailViewModel {
    
    var weatherInfo: WeatherInfo! = WeatherInfo() {
        didSet {
            setupWeatherSummaryList(weatherInfo: weatherInfo)
            setUpSummeryProfile(weatherInfo: weatherInfo)
        }
    }

    var summeryProfile = SummeryProfile()
    var weatherSummaryList = [SummaryInfo]()
    
    let HEADER_HIGHT: CGFloat = 136
    let CELL_HEIGHT: CGFloat = 100
    
    
    /// This method will populate values in SummeryProfile from WeatherInfo
    ///
    /// - Parameter weatherInfo: weatherInfo object
    /// - Returns: get values from WeatherInfo as SummeryProfile
    func setUpSummeryProfile(weatherInfo: WeatherInfo) {
        
        summeryProfile = SummeryProfile(city: weatherInfo.weatherInfoCityName,
                       temprature: String(weatherInfo.weatherInfoTemprature),
                       minTemprature: String(weatherInfo.weatherInfoMinTemprature),
                       maxTemprature: String(weatherInfo.weatherInfoMaxTemprature))
        
    }
    
    /// This method will populated displayer list of weatherSummary
    ///
    /// - Parameter weatherInfo: array of weatherSummaryList of type SummaryInfo
    func setupWeatherSummaryList(weatherInfo: WeatherInfo) {
        
        //Get time zone offline with the help of LatLongToTimezone Library for showing sunset and sunrise 
        //respective to country time zone
        let timeZone = Date.getTimeZoneByCoordinate(lattitude: weatherInfo.coordinate.lattitude,
                                                    longitude: weatherInfo.coordinate.longitude)
        
        //Get sunset display time with timeZone got from coordinates
        let sunset =  Date(timeIntervalSince1970: weatherInfo.weatherInfoSystemSunset as TimeInterval).getTime(withTimeZone: timeZone)
        
        //Get surise display time with timeZone got from coordinates
        let surise =  Date(timeIntervalSince1970: weatherInfo.weatherInfoSystemSunrise as TimeInterval).getTime(withTimeZone: timeZone)
        
        weatherSummaryList = [SummaryInfo(propertyName: SummaryProperies.SUNSET, propertyValue: sunset),
                              SummaryInfo(propertyName: SummaryProperies.SUNRISE, propertyValue: surise),
                              SummaryInfo(propertyName: SummaryProperies.CONDITION, propertyValue: weatherInfo.weatherCondition[0].weatherConditionDescription.capitalizeFirst()),
                              SummaryInfo(propertyName: SummaryProperies.WIND, propertyValue: "w \(weatherInfo.weatherInfoWindSpeed) m/hr"),
                              SummaryInfo(propertyName: SummaryProperies.HUMIDITY, propertyValue: "\(weatherInfo.weatherInfoHumidity)%"),
                              SummaryInfo(propertyName: SummaryProperies.VISIBLITY, propertyValue: "\(weatherInfo.weatherInfoVisiblity) m"),
                              SummaryInfo(propertyName: SummaryProperies.PRESSURE, propertyValue: "\(weatherInfo.weatherInfoPressure) hPa")
        ]
    }
}

//MARK: Weather Summary related models to display data on detail screen
extension WeatherDetailViewModel {
    
    struct SummeryProfile {
        var city = "" {
            didSet {
                if !city.isEmpty {
                   city =  city.capitalizeFirst()
                }
            }
        }
        var temprature = ""
        var minTemprature = ""
        var maxTemprature = ""
        
        var tempratureDisplay: String {
            return temprature + SpecialCharacteres.DEGREE
        }
        var minMaxTempDisplay: String {
            return minTemprature + SpecialCharacteres.DEGREE + SpecialCharacteres.SPACES +
                   SpecialCharacteres.SLASH + SpecialCharacteres.SPACES +
                   maxTemprature + SpecialCharacteres.DEGREE
        }
    }
    
    struct SummaryInfo {
        var propertyName = ""
        var propertyValue = ""
    }
    
    struct SummaryProperies {
        static let SUNSET = "Sunset"
        static let SUNRISE = "Sunrise"
        static let CONDITION = "Weather Condition"
        static let WIND = "Wind"
        static let HUMIDITY = "Humidity"
        static let VISIBLITY = "Visiblity"
        static let PRESSURE = "Pressure"
    }
}
