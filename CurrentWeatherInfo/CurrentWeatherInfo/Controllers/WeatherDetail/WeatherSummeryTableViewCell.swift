//
//  WeatherSummeryTableViewCell.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 21/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import UIKit

class WeatherSummeryTableViewCell: UITableViewCell {

    @IBOutlet weak var labelPropertyName: UILabel!
    @IBOutlet weak var labelPropertyValue: UILabel!
    
    //summaryInfo model object is responsible for ui of present cell
    var summaryInfo: WeatherDetailViewModel.SummaryInfo! {
        didSet {
            labelPropertyName.text = summaryInfo.propertyName
            labelPropertyValue.text = summaryInfo.propertyValue
        }
    }
    
    static let IDENTIFIER = "WeatherSummeryTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
}
