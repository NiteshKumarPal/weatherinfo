//
//  HeaderTemratureTableViewCell.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 21/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import UIKit

class HeaderTempratureTableViewCell: UITableViewCell {

    @IBOutlet weak var labelCityCountryName: UILabel!
    @IBOutlet weak var labelTemprature: UILabel!
    @IBOutlet weak var labelMinMaxTemprature: UILabel!
    
    //summeryProfile model object is responsible for ui of present cell
    var summeryProfile: WeatherDetailViewModel.SummeryProfile! {
        didSet {
            labelCityCountryName.text = summeryProfile.city
            labelTemprature.text = summeryProfile.tempratureDisplay
            labelMinMaxTemprature.text = summeryProfile.minMaxTempDisplay
        }
    }
    
    static let IDENTIFIER = "HeaderTempratureTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
