//
//  WeatherDetailViewController.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 20/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import UIKit

class WeatherDetailViewController: UIViewController {
    
    @IBOutlet weak var tableViewWeatherDetail: UITableView!
    
    let weatherDetailViewModel = WeatherDetailViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initial UI setup
        uiSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func uiSetup() {
        tableViewWeatherDetail.separatorColor = UIColor.clear
        tableViewWeatherDetail.tableFooterView = UIView()
        tableViewWeatherDetail.estimatedRowHeight = weatherDetailViewModel.CELL_HEIGHT
    }
}

//MARK: TableView datasource and delegates
extension WeatherDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherDetailViewModel.weatherSummaryList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let weatherSummeryTableViewCell = tableView.dequeueReusableCell(withIdentifier: WeatherSummeryTableViewCell.IDENTIFIER, for: indexPath) as! WeatherSummeryTableViewCell
        
        weatherSummeryTableViewCell.summaryInfo = weatherDetailViewModel.weatherSummaryList[indexPath.row]
        
        return weatherSummeryTableViewCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return weatherDetailViewModel.HEADER_HIGHT
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let initiativeDashboardSectionHeader = tableView.dequeueReusableCell(withIdentifier: HeaderTempratureTableViewCell.IDENTIFIER)
            as! HeaderTempratureTableViewCell
        
        initiativeDashboardSectionHeader.summeryProfile = weatherDetailViewModel.summeryProfile
        
        return initiativeDashboardSectionHeader
    }
}
