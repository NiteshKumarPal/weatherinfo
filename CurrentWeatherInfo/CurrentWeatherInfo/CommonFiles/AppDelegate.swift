//
//  AppDelegate.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 20/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        setupSwiftLoaderConfiguration()
        return true
    }
    
    //This method will setup swift loader UI configuration
    func setupSwiftLoaderConfiguration() {
        var config : SwiftLoader.Config = SwiftLoader.Config()
        let swiftLoaderSize: CGFloat = 100
        let alphaValue: CGFloat = 0.5
        config.size = swiftLoaderSize
        config.spinnerColor = .darkGray
        config.foregroundColor = .clear
        config.foregroundAlpha = alphaValue
        config.backgroundColor = .clear
        SwiftLoader.setConfig(config: config)
    }
}

