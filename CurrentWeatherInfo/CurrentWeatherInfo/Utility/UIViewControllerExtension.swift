//  UIViewControllerExtension.swift
//  Version1

import Foundation
import UIKit

extension UIViewController {
    
    // Show Alert Menu On Any Screen with handler
    /**
     Alert with message and handler
     
     - parameter message: message to be shown
     - parameter handler: handle to handle task
     */
    func showAlertWith(message: String,
                       buttonTitle: String = CommonAlertTexts.OK,
                       handler: @escaping (UIAlertAction) -> Void) -> UIAlertController {
        
        let alertVC = UIAlertController(title: "", message: message,
                                        preferredStyle: UIAlertControllerStyle.alert)
        alertVC.addAction(UIAlertAction(title: CommonAlertTexts.OK,
                                        style: UIAlertActionStyle.cancel, handler: handler))
        self.present(alertVC, animated: true, completion: nil)
        
        return alertVC
    }
    
    /**
     Alert with message, two buttons and handler on one button
     
     - parameter message:              message to be shown
     - parameter buttonFirstTitle:     title for 1st button
     - parameter buttoneSecondTitle:   title for 2nd button
     - parameter handlerOnFirstButton: handle to handle task
     */
    func showAlertWithTwoAction(message : String,
                                buttonFirstTitle: String,
                                buttoneSecondTitle: String,
                                handlerOnFirstButton: @escaping ((UIAlertAction) -> Void)) -> UIAlertController {
        
        let alertVC = UIAlertController(title: "", message: message,
                                        preferredStyle: UIAlertControllerStyle.alert)
        
        alertVC.addAction(UIAlertAction(title: buttonFirstTitle,
                                        style: UIAlertActionStyle.default, handler: handlerOnFirstButton))
        
        alertVC.addAction(UIAlertAction(title: buttoneSecondTitle,
                                        style: UIAlertActionStyle.default, handler: nil))
        
        self.present(alertVC, animated: true, completion: nil)
        
        return alertVC
    }
    
    // Show Alert Menu from Any Controller
    /**
     Alert with message only
     
     - parameter message: message to be shown
     */
    func showAlertWith(message : String) {
        let alert = UIAlertController(title: "", message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: CommonAlertTexts.OK,
                                      style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    /**
     To hide keyboard on touch of view of the UIViewController
     
     - parameter shouldHide: flag to enable or disable the hiding of keyboard
     */
    func shouldHideKeyboardOnTouch(shouldHide: Bool) {
        if shouldHide {
            let tapGestureRecognizer =
                UITapGestureRecognizer.init(target: self,
                                            action: #selector(UIViewController.hideKeyboard))
            self.view.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    
    /// Hides keyboard
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    /**
     For adding refresh control on tableView
     
     - parameter tableView:      tableView object
     - parameter refreshControl: object of UIRefreshControl
     - parameter massage:        message to be shown for pull to refresh controller
     */
    func addRefreshControllerForScrollView(scrollView: UIScrollView!,
                                      refreshControl: UIRefreshControl!,
                                      massage: String = "Pull to refresh",
                                      selector: Selector!
        ) {
        
        refreshControl.attributedTitle = NSAttributedString(string: massage)
        refreshControl.addTarget(self, action: selector, for: UIControlEvents.valueChanged)
        scrollView.addSubview(refreshControl)
    }
}
