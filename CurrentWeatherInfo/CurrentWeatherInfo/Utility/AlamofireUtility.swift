//  AlomFireUtility.swift

import Foundation
import Alamofire

var AlamofireUtilityInstance : AlamofireUtility { return AlamofireUtility() }
class AlamofireUtility {
    
    var manager: SessionManager!
    var request: URLRequest!

    let timeInterval: TimeInterval = 30.0
    
    init() {
        let instance: Alamofire.SessionManager!
        
        let config = URLSessionConfiguration.ephemeral
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.urlCache = nil
        config.timeoutIntervalForRequest = timeInterval
        instance = Alamofire.SessionManager(configuration: config)
        manager = instance
    }
    
    /// This method is for general api call for get method
    ///
    /// - Parameters:
    ///   - url: url for api call
    ///   - parameters: parameter to be added in request as json body
    ///   - showLoader: bool value for showing loader
    ///   - success: closure for success with response as type Any?
    ///   - failure: closure for failure with error as type NSError?
    func getCallWith(url: String,
                      parameters: Any? = [:],
                      showLoader: Bool = true,
                      succeess: @escaping ((_ response: Any?)->()),
                      failure: @escaping (_ error: NSError?)->()) {
        
        //internet check before making any call
        ReachablityUtility.internetCheckDependentCallWith {
            
            //show loading animation
            if showLoader {
                SwiftLoader.show(animated: true)
            }
            
            self.manager.request(url).responseJSON(completionHandler: { (response) in
                
                //For hiding spinning loader
                if showLoader {
                     SwiftLoader.hide()
                }
                
                //clear the cache before the get call
                URLCache.shared.removeAllCachedResponses()
                
                if let error = response.result.error {
                    failure(error as NSError)
                } else {
                    printForDebugMode("allHeaderFields: \(response.response?.allHeaderFields)")
                    succeess(response.result.value as Any?)
                }
                
                //Need to expire for manager session, otherwise it creates caching issue.
                self.manager.session.invalidateAndCancel()
            })
        }

    }
}
