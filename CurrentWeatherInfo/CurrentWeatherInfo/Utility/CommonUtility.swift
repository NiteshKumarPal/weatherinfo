//
//  UIUtility.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 23/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import Foundation

/// This method is printing in debug mode only
///
/// - Parameter objectList: any type object to be printed
func printForDebugMode(_ objectList: Any?...){
    
    guard objectList.count > 0 else { return }
    
    if _isDebugAssertConfiguration() {
        
        for object in objectList {
            print(object ?? "No value")
        }
    }
}
