//  ReachablityUtility.swift

import Foundation
import UIKit
import Toast_Swift

extension UIWindow {
    
    /// This property will return recent viewcontriller on the screen
    public var visibleViewController: UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(viewController: self.rootViewController)
    }
    
    /// This method will return top most vc of passed viewController
    ///
    /// - Parameter vc: Container view controll which might have more view controller
    /// - Returns: top most viewCOntroller of given viewController
    public static func getVisibleViewControllerFrom(viewController: UIViewController?) -> UIViewController? {
        if let navigationController = viewController as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(viewController: navigationController.visibleViewController)
        } else if let tabBarController = viewController as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(viewController: tabBarController.selectedViewController)
        } else {
            if let presentedViewController = viewController?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(viewController: presentedViewController)
            } else {
                return viewController
            }
        }
    }
}

class ReachablityUtility {
    //MARK : Internet connectivity
    /**
     Check internet
     
     - parameter method: Handling method which will be called when internet is not available
     */
    static func internetCheckDependentCallWith(method : (()->())?){
        
        if IJReachability.isConnectedToNetwork() {
            method?()
            
        } else {
   
            appDelegate.window?.visibleViewController?.view.makeToast(ToastConstant.CHECK_INTERNET,
                                                                      duration: ToastConstant.TOAST_DURATION,
                                                                      position: .center)
        }
    }
    
    @objc static func notified() {
        print("notified")
    }
}
