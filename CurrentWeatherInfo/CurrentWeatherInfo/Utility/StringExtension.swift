//
//  StringExtension.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 23/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import Foundation

extension String {
    
    ///This method will capitalize first letter of string
    func capitalizeFirst() -> String {
        let firstIndex = self.index(startIndex, offsetBy: 1)
        return self.substring(to: firstIndex).capitalized + self.substring(from: firstIndex).lowercased()
    }
    
}
