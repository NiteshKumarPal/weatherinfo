//
//  ModelApiUtility.swift
//  CurrentWeatherInfo
//
//  Created by Nitesh Kumar Pal on 21/04/17.
//  Copyright © 2017 Pioneer. All rights reserved.
//

import Foundation

let ModelApiUtilityInstance = ModelApiUtility()

class ModelApiUtility {
    
    /// This method is fetching weather info by city ids fetched from server
    ///
    /// - Parameters:
    ///   - cityId: given city ids
    ///   - completionWithWeatherInfo: completion handler is for handling data when response is received
    ///                                with parsed data, and updating the ui
    func getWeatherInfoListForCities(cityId: String = "4163971,2147714,2174003",
                                     completionWithWeatherInfo: (([WeatherInfo]?)->())?) {
        
        AlamofireUtilityInstance.getCallWith(url: URLs.CITY_WEATHER_API(cityId: cityId),
                                             showLoader: true,
                                             succeess: { (response) in
                                                
                                                if let weatherResponse = response as? [String : Any],
                                                   let weatherListResponse = weatherResponse[WeatherApiKeys.LIST],
                                                   let weatherList = ModelParserUtility.parseModelWithArrayWithNoKey(modelResponse: weatherListResponse as! [[String : Any]], modelClass: WeatherInfo.self) {
                                                    
                                                    printForDebugMode("response ", weatherList)
                                                    completionWithWeatherInfo?(weatherList)
                                                }
        },
                                             failure: { (error) in
        
        })
    }
}
