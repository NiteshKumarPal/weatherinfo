# Weather Info App
===================


Install Xcode version - 8.2.1 with swift3 

Goto CurrentWeatherInfo/ and install pod:

Added following Pods: 

* pod 'Alamofire', '~> 4.3' 
* pod 'Toast-Swift', '~> 2.0' 
* pod 'ObjectMapper', '~> 2.2' 
* pod 'FLAnimatedImage', '~> 1.0.12'
* pod 'LatLongToTimezone', '~> 1.1'


Things covered:

* Used MVVM design pattern
* UI for showing weather info of 3 city with given ids (4163971, 2147714, 2174003) 
* UI for showing detail weather info on tap of any city  
* webservice api integration with created api key on openweathermap.org 
* displayed fetched data on UI 
* used library for showing sunset and sunrise respective to country based on timezone 
* added blur effect for background


Enhancements:

* can be added 3d touch for tapping on any city, to quickly see its details
* as per weather condition background images can be changed
* refresh button on weather detail screen for refreshing current details
* can be added test cases